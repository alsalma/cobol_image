# cobol_image

https://hub.docker.com/r/sinenomine/cobol-s390x 

DID NOT WORK

https://hub.docker.com/r/frankr85/cobol 

Can compile but can not execute compiled binary

## To build

docker build -t my-cobol-image .

## To run

docker run -t -i --rm my-cobol-image bash

## Code Analysis Tools

https://owasp.org/www-community/Source_Code_Analysis_Tools


### Open source options for COBOL

https://www.hcltechsw.com/appscan/offerings/source

https://www.sonarsource.com/products/sonarcloud/

https://www.veracode.com

https://sourceforge.net/projects/visualcodegrepp/

